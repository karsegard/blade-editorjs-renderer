@props(['blocks'=>['blocks'=>[]]])
@php
if (is_string($blocks)){
    $blocks = json_decode($blocks,true);
}
@endphp
@if (is_array($blocks))
    @foreach ($blocks['blocks'] as $block)

        @php
            $component = 'blade-ejs::' . $block['type'];
        @endphp

        <x-dynamic-component :component="$component" :block="$block" />

    @endforeach
@endif
