<?php
// config for KDA/Blade\EJSRenderer
return [
    'views'=> [
        'ejs-block-paragraph'=>'blade-ejs::paragraph',
        'ejs-block-raw'=>'blade-ejs::raw',
        'ejs-block-render'=>'blade-ejs::render',
        'ejs-block-image'=>'blade-ejs::image',
        'ejs-block-header'=>'blade-ejs::header',
        'ejs-block-list'=>'blade-ejs::list',
        'ejs-block-table'=>'blade-ejs::table',
        'ejs-block-embed'=>'blade-ejs::embed',
    ],
];
