<?php

namespace KDA\Blade\EJSRenderer\Components;

use Illuminate\View\Component;

class Block extends Component
{

    public $block;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($block=null)
    {
        $this->block = $block;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return 'you should not use this component directly';
    }
}
