<?php

namespace KDA\Blade\EJSRenderer\Components;

use Illuminate\View\Component;

class Nested extends Component
{

    public $blocks;

    public $renderArticle;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($blocks=[],$renderArticle=true)
    {

        $this->blocks = $blocks;
        $this->renderArticle = $renderArticle;
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.content-block.nested-block');
    }
}
