<?php

namespace KDA\Blade\EJSRenderer\Components;

use Illuminate\View\Component;

class Raw extends Block
{

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(config('kda.blade-editorjs-renderer.views.ejs-block-raw'));
    }
}
