<?php

namespace KDA\Blade\EJSRenderer\Components;

use Illuminate\View\Component;

class Header extends Block
{
    
    public $block;
    public $offset;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($block=null,$offset=0)
    {
        $this->block = $block;
        $this->offset = $offset;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view(config('kda.blade-editorjs-renderer.views.ejs-block-header'));
    }
}
