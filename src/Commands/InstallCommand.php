<?php

namespace KDA\Blade\EJSRenderer\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:blade-editorjs-renderer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install blade-editorjs-renderer files';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        
    }
}
