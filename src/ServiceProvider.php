<?php
namespace KDA\Blade\EJSRenderer;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasComponentNamespaces;

    protected $viewNamespace = 'blade-ejs';
    protected $publishViewsTo = 'vendor/blade-ejs';

    protected $componentNamespaces = [
        '\KDA\Blade\EJSRenderer\Components'=> 'blade-ejs'
    ];

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/blade-editorjs-renderer.php'  => 'kda.blade-editorjs-renderer'
    ];
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
